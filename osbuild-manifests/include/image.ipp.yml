version: '2'
mpp-vars:
  extra_tree_content:
    - mpp-if: use_luks and luks_auto_unlock
      then:
        from: input://extra-tree/luks-key
        to: tree:///usr/.auto-unlock-key
  dracut_install:
    mpp-join:
      - mpp-eval: dracut_install
      - mpp-if: use_luks and luks_auto_unlock
        then:
          - /usr/.auto-unlock-key
  kernel_opts:
    mpp-join:
      - - ro
        - loglevel=$kernel_loglevel
        - mpp-if: use_efi_runtime
          then: efi=runtime
      - mpp-eval: kernel_opts
      - mpp-if: use_luks
        then:
          - rd.luks.uuid=$luks_uuid
          - rd.luks.options=discard
          - mpp-if: luks_auto_unlock
            then: rd.luks.key=$luks_uuid=/usr/.auto-unlock-key
pipelines:
# Some variables need to be written to files, do that here
- name: extra-tree-content
  build: name:build
  stages:
  - type: org.osbuild.copy
    inputs:
      inlinefile:
        type: org.osbuild.files
        origin: org.osbuild.source
        mpp-embed:
          id: luks-key
          text: $luks_passphrase
    options:
      paths:
        - from:
            mpp-format-string: input://inlinefile/{embedded['luks-key']}
          to: tree:///luks-key
- mpp-import-pipelines:
    path: image-$image_type.ipp.yml
- name: image
  build: name:build
  stages:
  - type: org.osbuild.truncate
    options:
      filename: disk.img
      size:
        mpp-eval: image.size
  - type: org.osbuild.sfdisk
    devices:
      device:
        type: org.osbuild.loopback
        options:
          filename: disk.img
    options:
      mpp-format-json: '{image.layout}'
  - type: org.osbuild.mkfs.fat
    devices:
      device:
        type: org.osbuild.loopback
        options:
          filename: disk.img
          start:
            mpp-eval: image.layout['efi'].start
          size:
            mpp-eval: image.layout['efi'].size
    options:
      label: ESP
      volid: 7B7795E7
  - type: org.osbuild.mkfs.ext4
    devices:
      device:
        type: org.osbuild.loopback
        options:
          filename: disk.img
          start:
            mpp-eval: image.layout['boot'].start
          size:
            mpp-eval: image.layout['boot'].size
    options:
      uuid:
        mpp-eval: bootfs_uuid
      label: boot
  - mpp-if: use_luks
    then:
      type: org.osbuild.luks2.format
      devices:
        device:
          type: org.osbuild.loopback
          options:
            filename: disk.img
            start:
              mpp-eval: image.layout['root'].start
            size:
              mpp-eval: image.layout['root'].size
            lock: true
      options:
        passphrase:
          mpp-eval: luks_passphrase
        uuid:
          mpp-eval: luks_uuid
        label: luks-rootfs
        pbkdf:
          method: argon2i
          memory: 32
          parallelism: 1
          iterations: 4
        integrity:
          mpp-if: luks_use_integrity
          then: hmac-sha256
  - mpp-if: use_luks
    then:
      type: org.osbuild.lvm2.create
      devices:
        luks:
          type: org.osbuild.loopback
          options:
            filename: disk.img
            start:
              mpp-eval: image.layout['root'].start
            size:
              mpp-eval: image.layout['root'].size
        device:
          type: org.osbuild.luks2
          parent: luks
          options:
            passphrase:
              mpp-eval: luks_passphrase
      options:
        volumes:
          - name: root
            extents: 100%FREE
  - type: org.osbuild.mkfs.ext4
    devices:
      luks:
        mpp-if: use_luks
        then:
          type: org.osbuild.loopback
          options:
            filename: disk.img
            start:
              mpp-eval: image.layout['root'].start
            size:
              mpp-eval: image.layout['root'].size
      lvm:
        mpp-if: use_luks
        then:
          type: org.osbuild.luks2
          parent: luks
          options:
            passphrase:
              mpp-eval: luks_passphrase
      device:
        mpp-if: use_luks
        then:
          type: org.osbuild.lvm2.lv
          parent: lvm
          options:
            volume: root
        else:
          type: org.osbuild.loopback
          options:
            filename: disk.img
            start:
              mpp-eval: image.layout['root'].start
            size:
              mpp-eval: image.layout['root'].size
    options:
      uuid:
        mpp-eval: rootfs_uuid
      label: root
  - type: org.osbuild.copy
    inputs:
      tree:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:image-tree
      build-tree:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:build
      extra-tree:
        mpp-if: "'extra_image_source_' + image_type in locals()"
        then:
          type: org.osbuild.tree
          origin: org.osbuild.pipeline
          references:
            - mpp-format-string: "name:{locals().get('extra_image_source_' + image_type)}"
    options:
      paths:
        mpp-join:
        - - from: input://tree/
            to: mount://root/
        - mpp-eval: locals().get('extra_image_copy_' + image_type, [])
    devices:
      efi:
        type: org.osbuild.loopback
        options:
          filename: disk.img
          start:
            mpp-eval: image.layout['efi'].start
          size:
            mpp-eval: image.layout['efi'].size
      boot:
        type: org.osbuild.loopback
        options:
          filename: disk.img
          start:
            mpp-eval: image.layout['boot'].start
          size:
            mpp-eval: image.layout['boot'].size
      root:
        mpp-if: use_luks
        then:
          type: org.osbuild.lvm2.lv
          parent: root-luks
          options:
            volume: root
        else:
          type: org.osbuild.loopback
          options:
            filename: disk.img
            start:
              mpp-eval: image.layout['root'].start
            size:
              mpp-eval: image.layout['root'].size
      root-raw:
        mpp-if: use_luks
        then:
          type: org.osbuild.loopback
          options:
            filename: disk.img
            start:
              mpp-eval: image.layout['root'].start
            size:
              mpp-eval: image.layout['root'].size
      root-luks:
        mpp-if: use_luks
        then:
          type: org.osbuild.luks2
          parent: root-raw
          options:
            passphrase:
              mpp-eval: luks_passphrase
    mounts:
    - name: root
      type: org.osbuild.ext4
      source: root
      target: /
    - name: boot
      type: org.osbuild.ext4
      source: boot
      target: /boot
    - name: efi
      type: org.osbuild.fat
      source: efi
      target: /boot/efi
  - mpp-if: use_luks
    then:
      type: org.osbuild.lvm2.metadata
      devices:
        luks:
          type: org.osbuild.loopback
          options:
            filename: disk.img
            start:
              mpp-eval: image.layout['root'].start
            size:
              mpp-eval: image.layout['root'].size
        device:
          type: org.osbuild.luks2
          parent: luks
          options:
            passphrase:
              mpp-eval: luks_passphrase
      options:
        vg_name: osbuild
        creation_host: osbuild
        description: "Built with osbuild"
- name: qcow2
  build: name:build
  stages:
  - type: org.osbuild.qemu
    inputs:
      image:
        type: org.osbuild.files
        origin: org.osbuild.pipeline
        references:
          name:image:
            file: disk.img
    options:
      filename: disk.qcow2
      format:
        type: qcow2
        compat: '1.1'

- name: container
  build: name:build
  stages:
  - type: org.osbuild.oci-archive
    inputs:
      base:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:rootfs
    options:
      filename: container.tar
      architecture: $arch
      config:
        Cmd:
        - "/usr/bin/bash"

# We need a smaller fstab for the non-partitioned case
- name: ext4-fstab
  build: name:build
  stages:
  # We copy /etc to get the right selinux context on the new file
  - type: org.osbuild.copy
    inputs:
      image-tree:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:image-tree
    options:
      paths:
      - from: input://image-tree/etc
        to: tree:///etc
  - type: org.osbuild.fstab
    options:
      filesystems:
        - uuid:
            mpp-eval: rootfs_uuid
          vfs_type: ext4
          path: /

- name: ext4
  build: name:build
  stages:
  - type: org.osbuild.truncate
    options:
      filename: rootfs.ext4
      size:
        mpp-eval: image.size
  - type: org.osbuild.mkfs.ext4
    devices:
      device:
        type: org.osbuild.loopback
        options:
          filename: rootfs.ext4
          start: 0
          size:
            mpp-format-int: "{int(image.size) // 512}"
    options:
      uuid:
        mpp-eval: rootfs_uuid
      label: root
  - type: org.osbuild.copy
    inputs:
      tree:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:image-tree
      fstab:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:ext4-fstab
    options:
      paths:
        mpp-join:
        - - from: input://tree/
            to: mount://root/
          - from: input://fstab/etc/fstab
            to: mount://root/etc/fstab
        - mpp-eval: locals().get('extra_image_copy_' + image_type, [])
    devices:
      root:
        type: org.osbuild.loopback
        options:
          filename: rootfs.ext4
          start: 0
          size:
            mpp-format-int: "{int(image.size) // 512}"
    mounts:
    - name: root
      type: org.osbuild.ext4
      source: root
      target: /

- name: tar
  build: name:build
  stages:
  - type: org.osbuild.tar
    inputs:
      tree:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:rootfs
    options:
      filename: rootfs.tar
      root-node: omit
